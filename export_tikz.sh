#!/bin/bash

# Make the header...
echo -e '\\documentclass{article}\n\\usepackage{graphics}\n\\usepackage{tikz}\n' > sketch_file.tex

# ... take the usetikzlibrary from input file...
cat $1 | grep usetikzlibrary >> sketch_file.tex

# ... continue making the header.
echo -e '\n\\pgfrealjobname{sketch}\n\\begin{document}\n\n' >> sketch_file.tex

# Add all 'tikzpicture' enviornments from input file.
cat $1 | awk '/\\begin{tikzpicture}/,/\\end{tikzpicture}/' >> sketch_file.tex

# Add 'beginpgfgraphicnamed' enviornments around each 'tikzpicture' enviornment.
sed s'/\\begin{tikzpicture}/\\beginpgfgraphicnamed\n\\begin{tikzpicture}/' sketch_file.tex > sketch_file_tmp.tex
sed s'/\\end{tikzpicture}/\\end{tikzpicture}\n\\endpgfgraphicnamed\n\n/' sketch_file_tmp.tex > sketch_file.tex
rm sketch_file_tmp.tex

# Add names for pgfgraphic.
ii=1
cat sketch_file.tex | sed s'/\\/\\\\/g' > sketch_file_tmp.tex
rm sketch_file.tex
cat sketch_file_tmp.tex | while read line
do
   newline=`echo $line | grep beginpgfgraphicnamed`
   if [ $newline ]
      then
         echo $line | sed s"/beginpgfgraphicnamed/beginpgfgraphicnamed{name_$ii}/" >> sketch_file.tex
         ii=$(( $ii + 1 ))
      else
         echo $line >> sketch_file.tex
   fi
done
rm sketch_file_tmp.tex
# Files will now have the names name_[NUMBER] where [NUMBER] is in the range 1.. number of 'tikzpicture' enviornments.

# End the document.
echo -e '\n\\end{document}' >> sketch_file.tex

# Execute the command to make pdf images.
jj=1
kk=`cat sketch_file.tex | grep beginpgfgraphicnamed | tail -1 | cut -c 28- | sed 's/}//'`
while [ $jj -le $kk ]
do
   pdflatex --jobname=name_$jj sketch_file.tex
   jj=$(( $jj + 1 ))
done

# Exit message.
echo -e '\n\nUnless you got caught in some pdflatex-error, your pictures should now be found in:'
jj=1
while [ $jj -le $kk ]
do
   echo name_$jj.pdf
   jj=$(( $jj + 1 ))
done

echo -e "\nThe tikz code is now found in the file sketch_file.tex, and your tikzpicture env.s can now be replaced with \\includegraphics{name_[NUMBER].pdf} in order of appearence in $1."

echo "Note also that this script does not handle comment env.s, hence remove everything in comment env.s to get the appropriate order of the images."
