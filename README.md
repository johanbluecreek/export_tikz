# export_tikz

## Description
Exports tikzpicture enviornments and replaces them with .pdf images.

Several journals do not accept tikzpicture enviornments, which means that one usually have to extract and compile the tikz pictures by hand.

This script moves the tikzpicture enviornments from the .tex file given as input and makes a backup file containing them (sketch\_file.tex) and generates .pdf files labeled name\_i.pdf with "i" ranging from 1 to the amount of tikzpicture enviornments, in order of appearance.

## Usage

Clone the project, see to it that export_tikz.sh is present in $PATH, then run

 `$ export_tikz.sh <full name of .tex file>`

in your project folder. Then substitute the tkizpicture enviornments with

 `\includegraphics{name_<NUMBER>.pdf}`

lines. A backup of your tikzpicture enviornments can be found in sketch_file.tex after the script has run.

Note that you should probably do this as the last step before submission, since it is easier to edit and see the layout when the tikzpictures are still in the main .tex.

## todo/known issues

* It does not exit gracefully when pdflatex catches some error.
* It does not verify whether or not it is overwriting files (name_i.pdf, name_i.log, sketch_file.tex, sketch_file_tmp.tex should be the only ones in potential danger.)
* It does not verify that the .tex file given as input actually exists or not.
